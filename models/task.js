const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
	name : {
		type : String,
		required: [true, "Name is required for this action"]
	},
	status : {
		type: String,
		default: "pending"
	}
})

module.exports = mongoose.model("Task", taskSchema);