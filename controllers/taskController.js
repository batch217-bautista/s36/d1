const Task = require("../models/task.js");

module.exports.getAllTasks = () => {

	return Task.find({}).then(result => {
		return result;
	});
}


module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name : requestBody.name
	})

	return newTask.save().then((task, error) =>{
		if(error) {
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removeTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removeTask;
		}
	})
}


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		} 
		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}

// [ACTIVITY]
// Process a GET request at the "/tasks/specificTask/:id" route using postman to get a specific task.

module.exports.specificTask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		} else {
			return result;
		}
	})
}

// Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

module.exports.completeStatus = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error);
			return false;
		}
		result.status = newStatus.status;

		return result.save().then((completeStatus, saveErr) =>{
			if(saveErr) {
				console.log(saveErr);
				return false;
			} else {
				return completeStatus;
			}
		})
	})
}