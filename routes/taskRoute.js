// It contains all endpoints for our application
const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController.js");

// [SECTION] Routes
// It is reponsible for defining or creating endpoints.
// All the business logic is done in the controller

router.get("/viewTasks", (req, res) => {
	// Invokes the "getAllTask" function from the "taskController.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

router.post("/addNewTask", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

router.delete("/deleteTask/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		res.send(resultFromController);
	});
})

router.put("/updateTask/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(
		resultFromController => {
			res.send(resultFromController);
		})
})

// [ACTIVITY]
// Process a GET request at the "/tasks/specificTask/:id" route using postman to get a specific task.

router.get("/specificTask/:id", (req, res) => {
	taskController.specificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;

// Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.

router.put("/changeStatus/:id/complete", (req, res) => {
	taskController.completeStatus(req.params.id, req.body).then(
		resultFromController => {
			res.send(resultFromController);
		})
})